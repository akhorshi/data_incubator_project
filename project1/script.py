"""
This script needs a couple of python libraries to be installed first. The
required python libraries are ase and ampforces which can be installed from

https://gitlab.com/ase/ase.git
git@bitbucket.org:akhorshi/ampforces.git

Moreover it needs two trajectory files 'training-images.traj' and
'test-images.traj' as well as a '.amp' file which all exist in my repository
link 1.
Link 1 also contains this full script. The length of this script is more than
10K characters, so please refer to 'script.py' in Link 1 to see the complete
version of this script.

"""

import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib.patches import FancyArrow, Circle
from ase.data import covalent_radii
from ase.io import Trajectory
from ase.data.colors import jmol_colors
from matplotlib import pyplot
from ase.calculators.emt import EMT
from ase import Atoms
import matplotlib.colors as colors
from ampforces import Amp
pyplot.set_cmap('Blues_r')
matplotlib.rcParams['font.size'] = 24.
matplotlib.rcParams.update({'figure.autolayout': True})

markersize = [5., 8.]  # Data points on contours

cutoff=2.13
atom_position = 1.36
line_slope = 1.5
half_x = 5.6
half_y = 4.3


def randomize_data(dataxyPairs, fraction, testDataxyIndices=None):

    if testDataxyIndices is None:
        trainingsize = int(fraction * len(dataxyPairs))
        testsize = len(dataxyPairs) - trainingsize
        testDataxyIndices = []
        while len(testDataxyIndices) < testsize:
            next = np.random.randint(len(dataxyPairs))
            if next not in testDataxyIndices:
                testDataxyIndices.append(next)
        testDataxyIndices.sort()

    trainingDataxyIndices = [index for index in range(len(dataxyPairs)) if index not in
                            testDataxyIndices]
    return trainingDataxyIndices, testDataxyIndices


def calculate_rmse(images, calc):
    rmse = 0.
    counter = 0
    for image in images:
        TrueForces = image.get_forces(apply_constraint=False)
        PredictedForces = calc.get_forces(image)
        for index in range(len(image)):
            counter += 1
            rmse += np.linalg.norm(np.array(TrueForces[index])-np.array(PredictedForces[index])) ** 2.
    rmse /= counter
    rmse = np.sqrt(rmse)
    return rmse


def makefig(figsize=(5., 5.), nh=1, nv=1,
            lm=0.1, rm=0.1, bm=0.1, tm=0.1, hg=0.1, vg=0.1,
            hr=None, vr=None):

    hr = np.array(hr, dtype=float) / np.sum(hr) if hr else np.ones(nh) / nh
    vr = np.array(vr, dtype=float) / np.sum(vr) if vr else np.ones(nv) / nv
    axwidths = (1. - lm - rm - (nh - 1.) * hg) * hr
    axheights = (1. - bm - tm - (nv - 1.) * vg) * np.array(vr)
    fig = pyplot.figure(figsize=figsize)
    axes = []
    bottompoint = 1. - tm
    for iv, v in enumerate(range(nv)):
        leftpoint = lm
        bottompoint -= axheights[iv]
        for ih, h in enumerate(range(nh)):
            ax = fig.add_axes((leftpoint, bottompoint,
                               axwidths[ih], axheights[iv]))
            axes.append(ax)
            leftpoint += hg + axwidths[ih]
        bottompoint -= vg
    return fig, axes


def plot_labels(ax, colorbar, title):
    ax.set_xlabel(r'x, $\AA$', fontsize=24.)
    ax.set_ylabel(r'y, $\AA$', fontsize=24.)
    colorbar.set_label(r'force, eV/$\AA$', fontsize=24.)
    ax.set_title(title, fontsize=28.)


def construct_atoms(x, y):
    atoms = Atoms(symbols='Pt3',
                  pbc=np.array([False, False, False], dtype=bool),
                  cell=np.array(
                  [[20.,  0.,  0.],
                   [0.,  20.,  0.],
                      [0.,  0.,  20.]]),
                  positions=np.array(
                  [[-atom_position, 0., 0.],
                   [atom_position, 0., 0.],
                      [x, y, 0.]]))
    atoms.set_calculator(EMT())
    return atoms


def calculate_true_F(x, y, index, cutoff):
    atoms = construct_atoms(x, y)
    if (atoms.get_distance(2, 0) > cutoff) and (atoms.get_distance(2, 1) > cutoff):
        RealForces = atoms.get_forces(apply_constraint=False)
        Fx = RealForces[index][0]
        Fy = RealForces[index][1]
    else:
        Fx = np.nan
        Fy = np.nan
    return (Fx, Fy)

def calculate_ml_F(calc, x, y, index, cutoff):
    atoms = construct_atoms(x, y)
    if (atoms.get_distance(2, 0) > cutoff) and (atoms.get_distance(2, 1) > cutoff):
        MLForces = calc.get_forces(atoms)
        Fx = MLForces[index][0]
        Fy = MLForces[index][1]
    else:
        Fx = np.nan
        Fy = np.nan
    return (Fx, Fy)


def generate_images(xys, cutoff):
    images = []
    for (x, y) in xys:
        atoms = construct_atoms(x, y)
        if (atoms.get_distance(2, 0) > cutoff) and (atoms.get_distance(2, 1) > cutoff):
            atoms.get_forces(apply_constraint=False)
            images.append(atoms)
    return images


def make_ax_a(fig, ax, dataxyPairs, trainingDataxy, testDataxy, gridxs, gridys,
              index, cutoff, scaling, title, contours=None):

    ax.set_xlim([np.min(gridxs), np.max(gridxs)])
    ax.set_ylim([np.min(gridys), np.max(gridys)])

    for (x, y) in dataxyPairs:
        (Fx, Fy) = calculate_true_F(x, y, index, cutoff)
        if (Fx is not np.nan) and (Fy is not np.nan):
            if (Fx**2. + Fy**2.)**0.5 > (10. ** (-8.)):
                dx = Fx / (Fx**2. + Fy**2.)**0.5
                dy = Fy / (Fx**2. + Fy**2.)**0.5
                arrow = FancyArrow(x=x,
                                   y=y,
                                   dx=scaling * dx / 1.8,
                                   dy=scaling * dy / 1.8,
                                   width=0.1 * scaling,
                                   length_includes_head=False,
                                   fill=True,
                                   alpha=0.7,
                                   facecolor='k',
                                   edgecolor='k',
                                   linewidth=0.02,
                                   linestyle='solid',
                                   head_width=0.5 * scaling,
                                   head_length=0.6 * scaling,
                                   shape='full',
                                   joinstyle='round',
                                   capstyle='round',
                                   )

                ax.add_patch(arrow)
                ax.plot(x, y, '.k', markersize=markersize[0])

    for (x, y) in testDataxy:
        ax.plot(x, y, 'wo', markersize=markersize[1])
    for (x, y) in trainingDataxy:
        ax.plot(x, y, 'ko', markersize=markersize[1])

    atoms = construct_atoms(0, 0)
    for atom in atoms[0:2]:
        color = jmol_colors[atom.number]
        radius = covalent_radii[atom.number]
        circle = Circle((atom.x, atom.y), radius, facecolor=color,
                        edgecolor='k', linewidth=1.)
        ax.add_patch(circle)

    ForceValues = np.empty(shape=(len(gridxs), len(gridxs[0])))

    for i in range(gridxs.shape[0]):
        for j in range(gridxs.shape[1]):
            x = gridxs[i][j]
            y = gridys[i][j]
            (Fx, Fy) = calculate_true_F(x, y, index, cutoff)
            if (Fx is np.nan) and (Fy is np.nan):
                ForceValues[i][j] = np.nan
            else:
                ForceValues[i][j] = (Fx**2. + Fy**2.)**0.5
    ForceValues = np.array(ForceValues)
    if contours == None:
        contour = ax.contourf(gridxs, gridys, ForceValues, extend='max')
    else:
        contour = ax.contourf(gridxs, gridys, ForceValues, contours, extend='max')
    colorbar = fig.colorbar(contour, ax=ax, extend='max')
    contours = contour.get_array()

    x_initial = (2 * atom_position + np.sqrt(4 * atom_position ** 2 + \
                                             4 * (line_slope ** 2 + 1) * (cutoff ** 2- atom_position ** 2))) / \
                (2 * (line_slope ** 2 + 1))
    y_initial = line_slope * x_initial
    y_final = half_y
    x_final = y_final / line_slope
    ax.plot([x_initial, x_final], [y_initial, y_final], 'r--', lw=4)

    plot_labels(ax, colorbar, title=title)

    return contours


def make_ax_b(fig, ax, calc, dataxyPairs, trainingDataxy, testDataxy, gridxs, gridys, index,
              cutoff, scaling, title, contours=None):

    ax.set_xlim([np.min(gridxs), np.max(gridxs)])
    ax.set_ylim([np.min(gridys), np.max(gridys)])

    for (x, y) in dataxyPairs:
        (Fx, Fy) = calculate_ml_F(calc, x, y, index, cutoff)
        if (Fx is not np.nan) and (Fy is not np.nan):
            if (Fx**2. + Fy**2.)**0.5 > (10. ** (-8.)):
                dx = Fx / (Fx**2. + Fy**2.)**0.5
                dy = Fy / (Fx**2. + Fy**2.)**0.5
                arrow = FancyArrow(x=x,
                                   y=y,
                                   dx=scaling * dx / 1.8,
                                   dy=scaling * dy / 1.8,
                                   width=0.1 * scaling,
                                   length_includes_head=False,
                                   fill=True,
                                   alpha=0.7,
                                   facecolor='k',
                                   edgecolor='k',
                                   linewidth=0.02,
                                   linestyle='solid',
                                   head_width=0.5 * scaling,
                                   head_length=0.6 * scaling,
                                   shape='full',
                                   joinstyle='round',
                                   capstyle='round',
                                   )

                ax.add_patch(arrow)
                ax.plot(x, y, '.k', markersize=markersize[0])

    for (x, y) in testDataxy:
        ax.plot(x, y, 'wo', markersize=markersize[1])
    for (x, y) in trainingDataxy:
        ax.plot(x, y, 'ko', markersize=markersize[1])

    atoms = construct_atoms(0, 0)
    for atom in atoms[0:2]:
        color = jmol_colors[atom.number]
        radius = covalent_radii[atom.number]
        circle = Circle((atom.x, atom.y), radius, facecolor=color,
                        edgecolor='k', linewidth=1.)
        ax.add_patch(circle)

    ForceValues = np.empty(shape=(len(gridxs), len(gridxs[0])))

    for i in range(gridxs.shape[0]):
        for j in range(gridxs.shape[1]):
            x = gridxs[i][j]
            y = gridys[i][j]
            (Fx, Fy) = calculate_ml_F(calc, x, y, index, cutoff)
            if (Fx is np.nan) and (Fy is np.nan):
                ForceValues[i][j] = np.nan
            else:
                ForceValues[i][j] = (Fx**2. + Fy**2.)**0.5
    ForceValues = np.array(ForceValues)

    if contours == None:
        contour = ax.contourf(gridxs, gridys, ForceValues, extend='max')
    else:
        contour = ax.contourf(gridxs, gridys, ForceValues, contours, extend='max')
    colorbar = fig.colorbar(contour, ax=ax, extend='max')
    contours = contour.get_array()



    plot_labels(ax, colorbar, title=title)

    return contours


def make_ax_c(fig, ax, calc, dataxyPairs, trainingDataxy, testDataxy, gridxs, gridys, index,
              cutoff, scaling, title):

    ax.set_xlim([np.min(gridxs), np.max(gridxs)])
    ax.set_ylim([np.min(gridys), np.max(gridys)])

    for (x, y) in dataxyPairs:
        (Fx, Fy) = calculate_ml_F(calc, x, y, index, cutoff)
        if (Fx is not np.nan) and (Fy is not np.nan):
            if (Fx**2. + Fy**2.)**0.5 > (10. ** (-8.)):
                ax.plot(x, y, '.k', markersize=markersize[0])

    for (x, y) in testDataxy:
        ax.plot(x, y, 'wo', markersize=markersize[1])
    for (x, y) in trainingDataxy:
        ax.plot(x, y, 'ko', markersize=markersize[1])

    atoms = construct_atoms(0, 0)
    for atom in atoms[0:2]:
        color = jmol_colors[atom.number]
        radius = covalent_radii[atom.number]
        circle = Circle((atom.x, atom.y), radius, facecolor=color,
                        edgecolor='k', linewidth=1.)
        ax.add_patch(circle)

    ErrorValues = np.empty(shape=(len(gridxs), len(gridxs[0])))

    for i in range(gridxs.shape[0]):
        for j in range(gridxs.shape[1]):
            x = gridxs[i][j]
            y = gridys[i][j]
            (TrueFx, TrueFy) = calculate_true_F(x, y, index, cutoff)
            (MLFx, MLFy) = calculate_ml_F(calc, x, y, index, cutoff)
            if (TrueFx is np.nan) and (TrueFy is np.nan):
                ErrorValues[i][j] = np.nan
            else:
                ErrorValues[i][j] = ((TrueFx - MLFx)**2. + (TrueFy - MLFy)**2.)**0.5

    ErrorValues = np.array(ErrorValues)
    MaskedErrorValues = np.ma.array(ErrorValues, mask=np.isnan(ErrorValues))
    MaskedGridxs = np.ma.array(gridxs, mask=np.isnan(ErrorValues))
    MaskedGridys = np.ma.array(gridys, mask=np.isnan(ErrorValues))
    MinMaskedErrorValues = np.min(MaskedErrorValues)
    MaxMaskedErrorValues = np.max(MaskedErrorValues)
    contour = ax.contourf(MaskedGridxs, MaskedGridys, MaskedErrorValues,
                          norm=colors.LogNorm(vmin=MinMaskedErrorValues,
                                              vmax=MaxMaskedErrorValues),
                            )
    colorbar = fig.colorbar(contour, ax=ax, extend='neither')



    plot_labels(ax, colorbar, title=title)


fig, axes = makefig(figsize=1.8 * np.array((3.* (half_x + 1.15), half_y)), nh=3, nv=1,
                    lm=0.032, rm=0.045, bm=0.105, tm=0.07, hg=0.038, vg=0.01)

no_x_step = 25
no_y_step = 25
dataxRange = np.arange(-half_x, half_x + 0.001, 2 * half_x / (no_x_step - 1))
datayRange = np.arange(-half_y, half_y + 0.001, 2 * half_y / (no_y_step - 1))

dataxs = []
datays = []
for y in datayRange:
    _y = []
    _x = []
    for x in dataxRange:
        _y.append(y)
        _x.append(x)
    datays.append(_y)
    dataxs.append(_x)
dataxs = np.array(dataxs)
datays = np.array(datays)

dataxyPairs = []
for x in dataxRange:
    for y in datayRange:      
        dataxyPairs.append((x, y))

training_images = Trajectory('training-images.traj')
test_images = Trajectory('test-images.traj')
trainingDataxy = [(image[2].x, image[2].y) for image in training_images]
testDataxy = [(image[2].x, image[2].y) for image in test_images]

gridxRange = np.arange(-half_x, half_x + 0.001, (1./6.) * half_x / (no_x_step - 1))
gridyRange = np.arange(-half_y, half_y + 0.001, (1./6.) * half_y / (no_y_step - 1))

gridxs = []
gridys = []
for y in gridyRange:
    _y = []
    _x = []
    for x in gridxRange:
        _y.append(y)
        _x.append(x)
    gridys.append(_y)
    gridxs.append(_x)
gridxs = np.array(gridxs)
gridys = np.array(gridys)

atom_index = 2


calc = Amp.load('Pt3-untrained-parameters.amp', label='calc7', cores=1)

plot = True

if plot:
    contours = make_ax_a(fig, axes[0], dataxyPairs, trainingDataxy, testDataxy, gridxs, gridys, atom_index, cutoff, scaling=0.3, title='EMT force field')

    make_ax_b(fig, axes[1], calc, dataxyPairs, trainingDataxy, testDataxy, gridxs, gridys, atom_index, cutoff, scaling=0.3, title='Machine-learning force field', contours=contours)

    pyplot.set_cmap('Oranges')
    make_ax_c(fig, axes[2], calc, dataxyPairs, trainingDataxy, testDataxy, gridxs, gridys, atom_index, cutoff, scaling=0.3, title='Norm of deviation of machine-learning from EMT')

    fig.savefig('real-ml-Pt3-abc.pdf')
    fig.savefig('real-ml-Pt3-abc.svg')
